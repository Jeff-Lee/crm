﻿using System.Collections.Generic;
using System.Threading.Tasks;
using IPSCRM.api.Base;
using IPSCRMCommon.Helpers;
using IPSCRMService;
using IPSCRMService.Services.DependencyInjection;
using IPSCRMService.Services.DependencyInjection.Shared;
using IPSCRMService.WebVM.Common;
using IPSCRMService.WebVM.CRM;
using IPSCRMService.WebVM.CRM.Filters;
using IPSCRMService.WebVM.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace IPSCRM.api
{
    [Route("api/[controller]")]
    public class CRMController : BaseCRMController
    {
        private readonly IAccountService accountService;
        private readonly IActivityService activityService;
        private readonly IDocumentAndFileService documentAndFileService;
        private readonly ITemplateService templateService;
        private readonly ICommentService commentService;
        private readonly IClientSearchTemplateService clientSearchTemplateService;

        public CRMController(IAccountService _accountService, IActivityService _activityService, IDocumentAndFileService _documentAndFileService, ITemplateService _templateService, ICommentService _commentService, IClientSearchTemplateService _clientSearchTemplateService)
        {
            this.accountService = _accountService;
            this.activityService = _activityService;
            this.documentAndFileService = _documentAndFileService;
            this.templateService = _templateService;
            this.commentService = _commentService;
            this.clientSearchTemplateService = _clientSearchTemplateService;


            this.logger = ApplicationLogging.LoggerFactory.CreateLogger<CRMController>();
        }

        //Ok
        //api/CRM/GetCalendar
        [HttpPost, Authorize]
        [Route("GetCalendar")]
        public async Task<ServiceResult<PagedResult<CalendarActivityVM>>> GetCalendar([FromBody] CalendarActivityFilterVM filter)
        {
            return await activityService.GetCalendar(filter, CurrentUser);
        }

        //Ok
        [HttpPost, Authorize]
        [Route("GetTaskList")]
        public async Task<ServiceResult<PagedResult<TaskListItemVM>>> GetTaskList([FromBody] ActivityListFilterVM filter)
        {
            return await activityService.GetTaskList(filter, CurrentUser);
        }

        [HttpPost, Authorize]
        [Route("Fake_GetTaskList")]
        public async Task<ServiceResult<PagedResult<TaskListItemVM>>> Fake_GetTaskList([FromBody] ActivityListFilterVM filter)
        {
            return  IPSCRMService.TestData.CRMApi.GetTaskList(filter);
        }

        //OK
        [HttpGet, Authorize]
        [Route("GetTaskDetails/{id}")]
        public async Task<ServiceResult<TaskDetailVM>> GetTaskDetails([FromRoute] int id)
        {
            return await activityService.GetTaskDetails(id, CurrentUser);
        }

        ///TODO
        [HttpPost, Authorize]
        [Route("ExportTaskList")]
        public async Task<IActionResult> ExportTaskList([FromBody] ActivityListFilterVM filter)
        {
            var result = await activityService.ExportTaskList(filter, CurrentUser);
            if (result.success)
            {
               return File(result.data, MimeTypeMap.GetMimeType(".xlsx"), "Task List.xlsx");
            }
            else
            {
                return StatusCode(500);
            }
        }

        ///TODO
        [HttpPost, Authorize]
        [Route("ExportHistoryList")]
        public async Task<IActionResult> ExportHistoryList([FromBody] ActivityListFilterVM filter)
        {
            var result = await activityService.ExportHistoryList(filter, CurrentUser);
            if (result.success)
            {
                return File(result.data, MimeTypeMap.GetMimeType(".xlsx"), "History List.xlsx");
            }
            else
            {
                return StatusCode(500);
            }
        }

        //OK
        [HttpPost, Authorize]
        [Route("CreateNote")]
        public async Task<ServiceResult<int>> CreateNote([FromBody] CreateNoteVM note)
        {
            return await activityService.CreateNote(note, CurrentUser);
        }


        ///TODO 5.1 -------------------------------------------------------
        //Change to, cc, DownloadAs", ->bcc (advi addr 10x)
        [HttpPost, Authorize]
        [Route("SendEmail")]
        public async Task<ServiceResult<int>> SendEmail([FromBody] EmailActivityClientVM email)
        {
            return await activityService.SendEmail(email, CurrentUser);
        }


        [HttpGet, Authorize]
        [Route("GetAdvancedClientSearchResult/{id}")]
        public async Task<ServiceResult<PagedResult<ClientListVM>>> GetAdvancedClientSearchResult([FromRoute] int id)
        {
            return await accountService.GetAdvancedClientSearchResult(id, CurrentUser);
        }

        //CDC

    }
}