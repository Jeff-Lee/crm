﻿using System.IO;
using AutoMapper;
using IPSCRMDb;
using IPSCRMService.Helpers;
using IPSCRMService.WebVM.CRM.Filters;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Extensions;
using Microsoft.EntityFrameworkCore.Query;
using System.Collections.Generic;
using System.Text;
using IPSCRMService.WebVM.CRM;
using IPSCRMDb.Model;
using IPSCRMService.WebVM.Shared;
using IPSCRMCommon.Helpers;
using IPSCRMCommon.Enums;
using IPSCRMService.Services.DependencyInjection.Shared;
using IPSCRMService.Services;
using IPSCRMService;
using IPSCRMService.WebVM.CDC;
using IPSCRMService.WebVM.CDC.Filters;
using LinqKit;
using IPSCRMCommon.Security;
using IPSCRMService.Services.DependencyInjection;
using System.Threading.Tasks;
using IPSCRMService.EmailServer;
using IPSCRMService.Services.Shared;
using Microsoft.Extensions.Configuration;
using IPSCRMCommon.EmailServer;
using Syncfusion.XlsIO;
using Syncfusion.DocIO;
using Syncfusion.DocIO.DLS;
using Syncfusion.DocIORenderer;
using Syncfusion.Pdf;

namespace IPSCRMService.Services
{
    public class ActivityService : BaseService, IActivityService
    {
        IConfiguration configuration;
        IEmailConfiguration emailConfiguration;
        public ActivityService(CRMContext _db, IMapper _mapper, IConfiguration _configuration, IEmailConfiguration _emailConfiguration) : base(_db, _mapper)
        {
            this.configuration = _configuration;
            this.emailConfiguration = _emailConfiguration;
            this.logger = ApplicationLogging.LoggerFactory.CreateLogger<ActivityService>();
        }
              

        //api/CRM/GetCalendar
        //NoTracking
        public async Task<ServiceResult<PagedResult<CalendarActivityVM>>> GetCalendar(CalendarActivityFilterVM filter, TokenUser currentUser)
        {
            try
            {
                var query = db.Activities.AsNoTracking().AsExpandable().Where(SecurityPredicates.SecureActivity(currentUser))
                    .Where(o => o.IsActive && o.StartDate >= filter.StartDate && o.EndDate <= filter.EndDate
                    && (filter.ActivityType & o.ActivityType) != 0 && (filter.Priority & o.Priority) != 0 && o.ActivityStatus != ActivityStatus.History);

                if (filter.SearchUserIds != null && filter.SearchUserIds.Count() > 0)
                {
                    query = query.Where(o => filter.SearchUserIds.Contains(o.CreatedBy) || filter.SearchUserIds.Contains(o.ScheduledFor.Id));
                }

                query = query.OrderByDescending(o => o.CreatedDate);// query = query.OrderByDescending(o => o.Id)
                //Ask about mapping
                return ServiceResult<PagedResult<CalendarActivityVM>>.AsSuccess(await base.GetPagedMappedResultAsync<CalendarActivityVM, Activity>(query, filter.Pager));
            }
            catch (Exception ex)
            {
                logger.Log(LogLevel.Critical, ex.Message);
                return ServiceResult<PagedResult<CalendarActivityVM>>.AsError(ErrorMessages.ServerError);
            }
        }

        //api/CRM/GetTaskList
        public async Task<ServiceResult<PagedResult<TaskListItemVM>>> GetTaskList(ActivityListFilterVM filter, TokenUser currentUser, bool historyOnly = false, bool myRecordOnly = false)
        {
            try
            {
                var query = db.Activities.AsNoTracking().
                    Include(o => o.CreatedUser).
                    Include(o => o.AccountSharedActivities).ThenInclude(s => s.Account).
                    Include(o => o.UserSharedActivities).ThenInclude(s => s.User).
                    Include(o => o.Favourites).
                    Include(o => o.ScheduledFor).
                    AsExpandable().Where(SecurityPredicates.SecureActivity(currentUser)).
                    Where(o => (filter.ActivityType & o.ActivityType) != 0 && (filter.Priority & o.Priority) != 0);

                //History Or Current
                if (filter.Id1.HasValue)
                {
                    query = query.Where(o => o.Id == filter.Id1.Value);
                }
                if (filter.AccountId.HasValue)
                {
                    query = query.Where(o => o.AccountSharedActivities.Any(a => a.AccountId == filter.AccountId));
                }

                if (historyOnly)
                {
                    query = query.Where(o => o.ActivityStatus == ActivityStatus.History);
                }
                else
                {
                    query = query.Where(o => (o.ActivityStatus & (ActivityStatus.Draft | ActivityStatus.InProgress | ActivityStatus.Active | ActivityStatus.Finished)) != 0);
                }

                //MyOnly
                if (myRecordOnly)
                {
                    query = query.Where(o => o.MyRecords.Exists(mr => mr.UserId == currentUser.UserId));
                }

                // This is for Export only
                if (filter.SelectedActivities != null && filter.SelectedActivities.Count() > 0)
                {
                    query = query.Where(o => filter.SelectedActivities.Contains(o.Id));
                }


                if (filter.CreatedDate.HasValue)
                {
                    query = query.Where(o => o.CreatedDate >= filter.CreatedDate.Value);
                }

                if (filter.TaskNo.HasValue)
                {
                    query = query.Where(o => o.Id == filter.TaskNo.Value);
                }
                if (filter.CreatedDate1.HasValue)
                {
                    query = query.Where(o => o.CreatedDate >= filter.CreatedDate1.Value);
                }

                if (filter.ActivityType1.HasValue)
                {
                    query = query.Where(o => (filter.ActivityType1.Value & o.ActivityType) != 0);
                }
                if (filter.Priority1.HasValue)
                {
                    query = query.Where(o => (filter.Priority1.Value & o.Priority) != 0);
                }
                ///????
                if (!string.IsNullOrWhiteSpace(filter.Regarding))
                {
                    query = query.Where(o => o.EmailSubject.Contains(filter.Regarding));
                }

                if (filter.Id1.HasValue)
                {
                    query = query.Where(o => o.Id == filter.Id1.Value);
                }

                if (filter.CreatedBy != null && filter.CreatedBy.Count() > 0)
                {
                    query = query.Where(o => filter.CreatedBy.Contains(o.CreatedBy));
                }

                if (filter.SearchUserIds != null && filter.SearchUserIds.Count() > 0)
                {
                    //query = query.Where(o => filter.SearchUserIds.Contains(o.ScheduledFor)); //??
                    query = query.Where(o => o.UserSharedActivities.Select(sa => sa.UserId).Any(t => filter.SearchUserIds.Contains(t))); //??
                }


                // Only start date ??
                if (filter.FromDate.HasValue)
                {
                    query = query.Where(o => o.StartDate >= filter.FromDate);
                }

                if (filter.ToDate.HasValue)
                {
                    query = query.Where(o => o.StartDate <= filter.ToDate);
                }

                query = query.OrderByDescending(o => o.CreatedDate);// query = query.OrderByDescending(o => o.Id)

                return ServiceResult<PagedResult<TaskListItemVM>>.AsSuccess(await base.GetPagedMappedResultAsync<TaskListItemVM, Activity>(query, filter.Pager, currentUser, TransformActivity));
            }
            catch (Exception ex)
            {
                logger.Log(LogLevel.Critical, ex.Message);
                return ServiceResult<PagedResult<TaskListItemVM>>.AsError(ErrorMessages.ServerError);
            }
        }


        //api/CRM/GetTaskDetails
        public async Task<ServiceResult<TaskDetailVM>> GetTaskDetails(int id, TokenUser currentUser)
        {
            try
            {

                ///TOTO Only active Comments and files ???
                ///
                var activity = await db.Activities.AsNoTracking().
                    Include(o => o.CreatedUser).
                    Include(o => o.AccountSharedActivities).ThenInclude(s => s.Account).
                    Include(o => o.UserSharedActivities).ThenInclude(s => s.User).
                    Include(o => o.Favourites).
                    Include(o => o.MyRecords).
                    Include(o => o.ScheduledFor).
                    Include(o => o.Files).
                    Include(o => o.Comments).
                    AsExpandable().
                    Where(SecurityPredicates.SecureActivity(currentUser)).
                    FirstOrDefaultAsync(o => o.Id == id);
                if (activity == null)
                    return ServiceResult<TaskDetailVM>.AsError(ErrorMessages.EntityNotFound("Task"));

                TransformActivity(activity, currentUser, true);
                var taskDetails = mapper.Map<Activity, TaskDetailVM>(activity);
                return ServiceResult<TaskDetailVM>.AsSuccess(taskDetails);
            }
            catch (Exception ex)
            {
                logger.Log(LogLevel.Critical, ex.Message);
                return ServiceResult<TaskDetailVM>.AsError(ErrorMessages.ServerError);
            }

        }


        /// <summary>
        /// TODO Print File
        /// </summary>
        //CRM/ExportHistoryList
        public async Task<ServiceResult<byte[]>> ExportHistoryList(ActivityListFilterVM filter, TokenUser currentUser)
        {
           
            try
            {
                filter.Pager = null;
                var r = await GetTaskList(filter, currentUser, true);

                if (!r.success)
                    return ServiceResult<byte[]>.AsError(r.message);


                ExcelEngine excelEngine = new ExcelEngine();
                IApplication application = excelEngine.Excel;
                IWorkbook workbook = application.Workbooks.Create(1);
                IWorksheet sheet = workbook.Worksheets[0];

                sheet[1, 1].Text = "Id";
                sheet[1, 2].Text = "Priority";
                sheet[1, 3].Text = "Regarding";
                sheet[1, 4].Text = "ActivityType";
                sheet[1, 5].Text = "CRM_Accounts";
                sheet[1, 6].Text = "ScheduledWith";
                sheet[1, 7].Text = "IsFavourite";
                sheet[1, 8].Text = "CreatedBy";
                sheet[1, 9].Text = "CreatedDate";

                int i = 2;
                foreach (var line in r.data.data)
                {
                    sheet[i, 1].Text = line.Id.ToString();
                    sheet[i, 2].Text = line.Priority.ToString();
                    sheet[i, 3].Text = line.Regarding;
                    sheet[i, 4].Text = line.ActivityType.ToString();
                    sheet[i, 5].Text = string.Join(',', line.CRM_Accounts.Select(o => o.FullName).ToArray());
                    sheet[i, 6].Text = string.Join(',', line.ScheduledWith.Select(o => o.FullName).ToArray());
                    sheet[i, 7].Text = line.IsFavourite ? "Y" : "N";
                    sheet[i, 8].Text = line.CreatedBy;
                    sheet[i, 9].Text = line.CreatedDate.ToString("YYYY/mmm/DD");
                    i++;
                }

                workbook.Version = ExcelVersion.Excel2016;
                MemoryStream ms = new MemoryStream();
                workbook.SaveAs(ms);
                ms.Position = 0;


                //TODO GENERATE PDF
                return ServiceResult<byte[]>.AsSuccess(ms.ToArray());
            }
            catch (Exception ex)
            {
                logger.Log(LogLevel.Critical, ex.Message);
                return ServiceResult<byte[]>.AsError(ErrorMessages.ServerError);
            }

           
        }

        private void TransformActivity(Activity activity, TokenUser currentUser, bool activeOnly = true)
        {
            if (activity.AccountSharedActivities != null && activity.AccountSharedActivities.Count() > 0)
                activity.AccountSharedActivities = activity.AccountSharedActivities.Where(o => o.IsActive == activeOnly).ToList();
            if (activity.UserSharedActivities != null && activity.UserSharedActivities.Count() > 0)
                activity.UserSharedActivities = activity.UserSharedActivities.Where(o => o.IsActive == activeOnly).ToList();
            if (activity.Favourites != null)
                activity.Favourites = activity.Favourites.Where(o => o.UserId == currentUser.UserId).ToList();
            if (activity.MyRecords != null)
                activity.MyRecords = activity.MyRecords.Where(o => o.UserId == currentUser.UserId).ToList();
            if (activity.Comments != null)
                activity.Comments = activity.Comments.Where(o => o.IsActive == activeOnly).OrderByDescending(o => o.CreatedDate).ToList();
            if (activity.Files != null)
                activity.Files = activity.Files.Where(o => o.IsActive == activeOnly).OrderBy(o => o.CreatedDate).ToList();
        }
    }


}
