using AutoMapper;
using IPSCRMCommon.Helpers;
using IPSCRMCommon.Security;
using IPSCRMDb;
using IPSCRMService.Automapper;
using IPSCRMService.Helpers;
using IPSCRMService.Services;
using IPSCRMService.WebVM.CRM;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using Newtonsoft.Json;
using System;
using System.IdentityModel.Tokens.Jwt;
using Xunit;
using Microsoft.Extensions.Configuration;
using IPSCRMService.WebVM;

namespace IPSCRMTest
{
    public class ClientUnitTest
    {
        private IMapper mapper;
        private ILogger logger;
        private IConfiguration configuration;

        public ClientUnitTest()
        {
            var config = TestMapperProfileConf.InitializeAutoMapper();
            //this.logger = new ConsoleLogger();
            this.mapper = config.CreateMapper();
            this.configuration = FakeConfigurationBuilder();
            config.AssertConfigurationIsValid();

            ApplicationLogging.LoggerFactory = new LoggerFactory().AddConsole(LogLevel.Error);
            //logger = ApplicationLogging.LoggerFactory.CreateLogger("Test_ClientUnitTest");

        }

        private IConfigurationRoot FakeConfigurationBuilder()
        {
            return new ConfigurationBuilder().Build();
        //.AddJsonFile("appsettings.json", optional: true)
        //.AddUserSecrets("e3dfcccf-0cb3-423a-b302-e3e92e95c128")
        //.AddEnvironmentVariables()
        //.Build();
        }
        [Fact]
        public void MapperTest()
        {
            SourceDb s1 = new SourceDb() { FirstName = "John", LastName = "Smith", Age = 37, PlaceOfBirth = "BB" };
            SourceDb s2 = new SourceDb() { FirstName = "John", LastName = "Smith", Age = 37, PlaceOfBirth = "BB" };


            Destination d1 = new Destination() { FirstName = "John", LastName = "Test", PlaceOfBirth = "Sydney", Ext = new DestinationExt() { Age = 31} };
            Destination d2 = new Destination() { FirstName = "John1", LastName = "Test1", PlaceOfBirth = "Sydney1", Ext = new DestinationExt() { Age = 32} };
            DestinationBase dbase = new DestinationBase() { FirstName = "PaulB", LastName = "TestB" };

            Destination2 dd = new Destination2() { FirstName = "Paul2", LastName = "Test2" };
            //mapper.Map<SourceDb, Destination>(s, d);
            //mapper.Map<SourceDb, DestinationBase>(s, dbase);
            //Assert.True(s.Age == 37);
            //Assert.True(s.FirstName == "Paul");
            //Assert.True(s.PlaceOfBirth == "Sydney");

            //mapper.Map<Destination,SourceDb>(d1,s1);
            //mapper.Map<DestinationBase,SourceDb>(dbase, s2);

            mapper.Map<SourceDb,Destination>(s1,d1);
            // mapper.Map<SourceDb, DestinationBase>(s2,dbase);
            mapper.Map<SourceDb, Destination2>(s1, dd);
            //mapper.Map<SourceDb, Destination2>(s1, dd);
            // mapper.Map<SourceDb, DestinationBase>(s2, dbase);

            //Assert.True(s.Age == 37);
            //Assert.True(s.FirstName == "Paul");
            //Assert.True(s.PlaceOfBirth == "Sydney");

        }


        [Fact]
        public void CreateClient()
        {
            /*
            var opt = new DbContextOptionsBuilder<CRMContext>().UseInMemoryDatabase(databaseName: "CreateClientDb").Options;
            var db = new CRMContext(opt);


            AccountService accService = new AccountService(db, configuration, mapper);
            var clientNew = AuxCreateAccount();
            // Where to get this, based on user crederntials ?
            //clientNew.DealerGroupId = 100;
            var result = accService.SaveNewClient(clientNew, new TokenUser(new JwtSecurityToken()));
           // var js = JsonConvert.SerializeObject(result, Formatting.Indented);
           // Assert.True(result.success);*/
        }

        #endregion
    }
}
