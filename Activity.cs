﻿using IPSCRMCommon.Enums;
using IPSCRMDb.Model.ExtentionTypes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace IPSCRMDb.Model
{


    public class Activity : IAuditableCUEntity, IAuditableDEntity, IDealerGroupId
    {
        public int Id { get; set; }

        public int DealerGroupId { get; set; }

        public ActivityType ActivityType { get; set; }

        public ActivityPriority Priority { get; set; }

        public ActivityStatus ActivityStatus { get; set; }

        [Column(TypeName = "Date")]
        public DateTime? StartDate { get; set; }

        [Column(TypeName = "Date")]
        public DateTime? EndDate { get; set; }

        [MaxLength(8)]
        public string StartTime { get; set; }

        [MaxLength(8)]
        public string EndTime { get; set; }

        public bool IsAllDay { get; set; }
               

        //[MaxLength(50)]
        public string Recurrence { get; set; }

        [MaxLength(2000)]
        public string Regarding { get; set; }

        public string ActivityDetails { get; set; }

        public bool IsActivity { get; set; }
               
        public int? ScheduledForId { get; set; }
        
        [MaxLength(100)]
        public string RecordManager { get; set; }

        public int? TemplateId { get; set; }

        [MaxLength(2000)]
        public string EmailTo { get; set; }

        [MaxLength(2000)]
        public string EmailFrom { get; set; }

        [MaxLength(2000)]
        public string EmailCc { get; set; }

        [MaxLength(2000)]
        public string EmailBcc { get; set; }

        [MaxLength(1000)]
        public string EmailCRMAccountIds { get; set; }
        
        [MaxLength(2000)]
        public string EmailSubject { get; set; }

        public string EmailBody { get; set; }

        public EmailType? EmailBodyType { get; set; }

        // Not necessary
        //public MailMergeDownloadType? MailMergeDownloadType { get; set; }

        // Not necessary
        //[MaxLength(10)]
        //public string MailMergeDownloadAs { get; set; }

        [MaxLength(30)]
        public string MailMergeDownloadFileName { get; set; }

        public int CommentCount { get; set; }

        public bool HasFile { get; set; }

        public bool IsCleared { get; set; }

        public int? ClearedBy { get; set; }

        public DateTime? ClearedDate { get; set; }


        #region IAuditableCU/DEntity
        public int CreatedBy { get; set; } // Unalloc. Emails -> change  select history My rec. -> change to advisorID
        public DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool IsActive { get; set; }
        public int? DeletedBy { get; set; }
        public DateTime? DeletedDate { get; set; }

        public virtual IPSUser CreatedUser { get; set; }
        public virtual IPSUser ModifiedUser { get; set; }
        public virtual IPSUser DeletedUser { get; set; }
        #endregion

        public virtual IPSUser ScheduledFor { get; set; }
        public virtual Template Template { get; set; }
        public virtual List<AccountSharedActivity> AccountSharedActivities { get; set; }
        public virtual List<UserSharedActivity> UserSharedActivities { get; set; }
        //public virtual List<SharedActivity> SharedActivity { get; set; }
        public virtual List<Comment> Comments { get; set; }
        public virtual List<File> Files { get; set; }
        public virtual List<ActivityMyRecord> MyRecords { get; set; }
        public virtual List<ActivityIsFavourite> Favourites { get; set; }

        public virtual IPSAdvisorGroup DealerGroup { get; set; }


    }
}
