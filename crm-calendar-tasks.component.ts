import { Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';

import { closest, createElement } from '@syncfusion/ej2-base';
import { DatePicker } from '@syncfusion/ej2-calendars';
import { EventSettingsModel, DayService, WeekService, MonthService, AgendaService, EventClickArgs, PopupOpenEventArgs, CellClickEventArgs, ScheduleComponent, NavigatingEventArgs } from '@syncfusion/ej2-ng-schedule';
import { FilterService, GridComponent, SelectionSettingsModel, FilterSettingsModel, RowSelectEventArgs, IFilterUI, Column, ColumnModel } from '@syncfusion/ej2-ng-grids';
import { DataManager } from '@syncfusion/ej2-data';
import { DropDownList } from '@syncfusion/ej2-ng-dropdowns';

import { MessageService } from '../../services/message.service';
import { UtilityService } from '../../services/utility.service';
import { GatewayService } from '../../services/gateway.service';
import { EventService } from '../../services/event.service';
import { ConfigService} from '../../services/config.service';
import { Activity, CalendarTask } from '../../models/data-model';

@Component({
  selector: 'app-crm-calendar-tasks',
  templateUrl: './crm-calendar-tasks.component.html',
  providers: [DayService, WeekService, MonthService, AgendaService],
  encapsulation: ViewEncapsulation.None
})
export class CrmCalendarTasksComponent implements OnInit, OnDestroy {

	public activityList: Activity[];
	public activityListOptions: any = {};

	public calendarTaskList: CalendarTask[];
    public calendarTaskListOptions: any = {};
    private currentCalendarYYYYMM: string;

    private subscription: Subscription;
    public filterOptions: FilterSettingsModel;

    constructor(
        private message: MessageService,
        private gateway: GatewayService,
        private utility: UtilityService,
        private event: EventService,
        public config: ConfigService
    ) {
      this.subscription = this.event.getEvent().subscribe(event => this.handleGlobalEvent(event));
    }

  ngOnInit() {
    // this.message.add('[Calendar & Tasks] page entered.');
    /*
    let utcTime = moment().utc().format();
    let testLocalDate = new Date('2019-02-06 00:00:00');
    console.log('today is', utcTime);
    console.log('today is', moment(utcTime).local().format("YYYY-MM-DD HH:mm:ss"));
    console.log('today is', moment.utc(testLocalDate).format());
    console.log('today is', moment(testLocalDate).local().format("YYYY-MM-DD HH:mm:ss"));
    */
    this.getClientList();
    this.clearActivitySearchOptions();
    this.clearCalendarSearchOptions(new Date());
    this.updateContents();
    this.initCustomGridFilters();

    this.filterOptions = {
        columns: [{ field: 'ClientNames', matchCase: false, operator: 'contains', predicate: 'and', value: '' },
                  { field: 'ScheduledWithNames', matchCase: false, operator: 'contains', predicate: 'and', value: '' }
                ]};
  }
  
  ngOnDestroy() {
    // this.message.add('[Calendar & Tasks] page closed.');
    this.subscription.unsubscribe();
  }

  handleGlobalEvent(event: any): void {
    //console.log(event);
    if (event != undefined) {
        if (event.type === 'page') {
            if (event.detail === 'update') {
                //console.log(event);
                this.updateContents();
            }
        }
        else if (event.type === 'data' && event.target === 'crm-calendar-task') {
            if (event.module === 'calendar') {
                this.calendarTaskListOptions.SearchUserIds = event.data;
                //console.log(event.data);
                this.getCalendarTaskList();
            }
            else if (event.module === 'task') {
                this.activityListOptions.SearchUserIds = event.data;
                //console.log(event.data);
                this.getActivityList();
            }
            else if (event.module === 'task-select-dates') {
                if (event.data != undefined && event.data != null) {
                    //console.log(event.data.split(' ')[0], event.data.split(' ')[1]);
                    this.activityListOptions.SearchStartDate = event.data.split(' ')[0];
                    this.activityListOptions.SearchEndDate = event.data.split(' ')[1];
                    this.getActivityList();
                }
            }
        }
    }
  }

  updateContents(): void {
  	this.getActivityList();
  	this.getCalendarTaskList();
  }

  searchCalendar(): void {
    this.clearCalendarSearchOptions(new Date());
    //console.log(this.calendarTaskListOptions);
    this.getCalendarTaskList();
  }

  searchActivities(): void {
    //console.log(this.activityListOptions);
    this.getActivityList();
  }

  clearCalendarSearchOptions(date: Date): void {
      /*
      let startDate = new Date();
      startDate.setHours(0,0,0,0)
      let endDate = new Date();
      console.log(this.utility.getISOStringFromDate(startDate), this.utility.getISOStringFromDate(endDate));
    */
   this.currentCalendarYYYYMM = date.getFullYear()+''+(date.getUTCMonth()+1);
   var firstDay = new Date(date.getFullYear(), date.getUTCMonth(), 1);
   firstDay.setHours(0,0,0,0);
   var lastDay = new Date(date.getFullYear(), date.getUTCMonth() + 1, 0);
   lastDay.setHours(0,0,0,0);
   //let startDate = this.utility.getDateFromUTCString(this.utility.getUTCStringFromDate(new Date));
   //console.log(firstDay, lastDay, this.utility.getISOStringFromDate(firstDay), this.utility.getISOStringFromDate(lastDay));
   //console.log(firstDay, lastDay, this.utility.getUTCStringFromDate(firstDay), this.utility.getUTCStringFromDate(lastDay));
   this.calendarTaskListOptions = {
        ActivityType: this.config.actionSearchTypes[0]['id'],
        Priority: this.config.prioritySearchTypes[0]['id'],
        StartDate: this.utility.getUTCStringFromDate(firstDay),
        EndDate: this.utility.getUTCStringFromDate(lastDay),
        SearchUserIds: [],
        SearchAccountIds: this.clientValues
    };
  }

  clearActivitySearchOptions(): void {
    this.activityListOptions = {
      ActivityType: this.config.actionSearchTypes[0]['id'],
      Priority: this.config.prioritySearchTypes[0]['id'],
      SearchUserIds: []
    };
  }

  getActivityList(): void {
    this.activityList = undefined;
  	this.gateway.getActivityList(this.activityListOptions)
  		.subscribe(activityList => {
  			this.activityList = activityList;
  			// TODO: normalize here
  		});
  }

  getCalendarTaskList(): void {
    this.calendarTaskList = undefined;
    // remove the existing calendar tasks
    this.calendarData.splice(0, this.calendarData.length);
    if (this.scheduleObj !== undefined) {
      this.scheduleObj.refreshEvents();
      this.scheduleObj.readonly = true;
    }
    //console.log(this.calendarTaskListOptions);
  	this.gateway.getCalendarTaskList(this.calendarTaskListOptions)
  		.subscribe(calendarTaskList => {
        this.calendarTaskList = calendarTaskList;
        //console.log(calendarTaskList);

        for (let calendarTask of this.calendarTaskList) {
          let object: Object = {
            Id: calendarTask.EventID,
            Subject: calendarTask.EventName,
            StartTime: calendarTask.StartDateStd,
            EndTime: calendarTask.EndDateStd,
            IsAllDay: calendarTask.IsAllDay,
            // RecurrenceID: undefined,
            // RecurrenceRule: undefined,
            // Location: 'Sydney',
            // Description: calendarTask.DisplayName,
            // StartTimezone: 'Australia/Sydney',
            // EndTimezone: 'Australia/Sydney'
          }
          this.calendarData.push(object);
        }

        //console.log(this.calendarData);
  		});
  }

  getCalendarTaskListNavi(): void {
    this.calendarData.splice(0, this.calendarData.length);
    this.gateway.getCalendarTaskList(this.calendarTaskListOptions)
        .subscribe(calendarTaskList => {
        this.calendarTaskList = calendarTaskList;
        //console.log(calendarTaskList);

        for (let calendarTask of this.calendarTaskList) {
            let object: Object = {
            Id: calendarTask.EventID,
            Subject: calendarTask.EventName,
            StartTime: calendarTask.StartDateStd,
            EndTime: calendarTask.EndDateStd,
            IsAllDay: calendarTask.IsAllDay,
            // RecurrenceID: undefined,
            // RecurrenceRule: undefined,
            // Location: 'Sydney',
            // Description: calendarTask.DisplayName,
            // StartTimezone: 'Australia/Sydney',
            // EndTimezone: 'Australia/Sydney'
            }
            this.calendarData.push(object);
        }
        if (this.calendarData.length > 0) {
            this.scheduleObj.refreshEvents();
        }
    });
  }
  openActivityDialog(activityId: number): void {
  	this.event.sendEvent({
  		type: 'dialog',
  		detail: 'activity',
        id: activityId,
        option: 'Note',
  	});
  }
  getClientList() {
    this.gateway.getClientList({})
    .subscribe(() => {
      //console.log(this.gateway.currentClientList);
    });
  }

  @ViewChild('grid')
  public gridObj: GridComponent;
  public gridSelectionOptions: SelectionSettingsModel = {
  	"checkboxOnly": true
  };
  public activityFilter: IFilterUI;
  public createdDateFilter: IFilterUI;
  public priorityFilter: IFilterUI;

  initCustomGridFilters(): void {
    let activityTypes = this.config.actionSearchTypes;
    this.activityFilter = {
      create:(args: { element: Element, column: Column }) => {
        let dtv: HTMLInputElement = document.createElement('input');
        dtv.id='ActivityType';
        return dtv;
      },
      write:(args: { element: Element, column: any }) => {
        let DropDownListObj: DropDownList = new DropDownList({
          dataSource: new DataManager(activityTypes),
          fields: this.config.actionSearchFields,
          placeholder: 'Select a value',
          popupHeight: '200px',
          change: function(e){
            //here we can filter the column while select value from Date Range picker
            var gridObj = (document.getElementsByClassName('e-grid')[0] as any).ej2_instances[0];
            if (e.value !== 31) {
              gridObj.filterByColumn('ActivityType','equal',e.value);
            } else {
              gridObj.removeFilteredColsByField('ActivityType');
            }
          },
        });
        DropDownListObj.appendTo('#ActivityType');
      }
    };

    let priorityTypes = this.config.prioritySearchTypes;
    this.priorityFilter = {
      create:(args: { element: Element, column: Column }) => {
        let dtv: HTMLInputElement = document.createElement('input');
        dtv.id='Priority';
        return dtv;
      },
      write:(args: { element: Element, column: any }) => {
        let DropDownListObj: DropDownList = new DropDownList({
          dataSource: new DataManager(priorityTypes),
          fields: this.config.prioritySearchFields,
          placeholder: 'Select a value',
          popupHeight: '200px',
          change: function(e){
            //here we can filter the column while select value from Date Range picker
            var gridObj = (document.getElementsByClassName('e-grid')[0] as any).ej2_instances[0];
            console.log(e.value);
            if (e.value !== 15) {
              gridObj.filterByColumn('Priority','equal',e.value);
            } else {
              gridObj.removeFilteredColsByField('Priority');
            }
          },
        });
        DropDownListObj.appendTo('#Priority');
      }
    };

    this.createdDateFilter = {
      create:(args: { element: Element, column: Column }) => {
        let dtv: HTMLInputElement = document.createElement('input');
        dtv.id='CreatedDateStd';
        return dtv;
      },
      write:(args: { element: Element, column: any }) => {
        let datePicker: DatePicker = new DatePicker({
          change: function(e){
            //here we can filter the column while select value from Date Range picker
            var gridObj = (document.getElementsByClassName('e-grid')[0] as any).ej2_instances[0];
            console.log(e.value);
            if (e.value !== null) {
              gridObj.filterByColumn('CreatedDateStd','equal',e.value);
            } else {
              gridObj.removeFilteredColsByField('CreatedDateStd');
            }
          },
        });
        datePicker.appendTo('#CreatedDateStd');
      }
    };

  }
  showGridColumnChooser(): void {
  	console.log(this.gridObj);
  	this.gridObj.columnChooserModule.openColumnChooser(10, 10);
  }
  rowSelected (args: RowSelectEventArgs) {
  	// TODO: checkbox event handling
  }
  rowClicked (e: any) {
  	const cell = closest(e.target, 'td');
  	if (cell && !cell.classList.contains('e-gridchkbox') && closest(cell, '.e-row')) {
      const cellIndex = parseInt(cell.getAttribute('aria-colindex'), 10);
      const rowIndex = parseInt(cell.parentElement.getAttribute('aria-rowindex'), 10);
      const data = this.gridObj.getCurrentViewRecords()[rowIndex];
      console.log(data['Id']);
      this.openActivityDialog(data['Id']);
    }
  }
  gridCreated (e: any) {
    // index 0 is the checkbox which doesn't have a filter.
    for( let i = 1; i < this.gridObj.columns.length; i++ ) {
      if( !(this.gridObj.columns[i] as ColumnModel).allowFiltering ) {
        (this.gridObj.getHeaderTable().getElementsByClassName('e-filterbarcell')[i].getElementsByClassName('e-fltrinputdiv')[0] as any).style.display='none'
      }
    }
  }

  @ViewChild('scheduleObj')
  public scheduleObj: ScheduleComponent;
  public selectedDate: Date = new Date();
  public dateFormat: string = "dd/MM/yyyy";
  public showQuickInfo: boolean = false;
  public readonly: boolean = false;
  public calendarData: Object[] = [];
  /*
  public calendarData: Object [] = [
    {
      Id: 1,
      Subject: 'Conference',
      StartTime: new Date(2018, 1, 7, 10, 0),
      EndTime: new Date(2018, 1, 7, 11, 0),
      IsAllDay: false
    },
    {
      Id: 2,
      Subject: 'Meeting - 1',
      StartTime: new Date(2018, 1, 15, 10, 0),
      EndTime: new Date(2018, 1, 16, 12, 30),
      IsAllDay: false
    },
    {
      Id: 3,
      Subject: 'Paris',
      StartTime: new Date(2018, 1, 13, 12, 0),
      EndTime: new Date(2018, 1, 13, 12, 30),
      IsAllDay: false
    },
    {
      Id: 4,
      Subject: 'Vacation',
      StartTime: new Date(2018, 1, 12, 10, 0),
      EndTime: new Date(2018, 1, 12, 10, 30),
      IsAllDay: false
    }
  ];
  */
  public eventSettings: EventSettingsModel = {
  	dataSource: this.calendarData,
  	fields: {
  		id: 'Id',
  		subject: { name: 'Subject' },
  		isAllDay: { name: 'IsAllDay' },
  		location: { name: 'Location' },
  		description: { name: 'Description' },
  		startTime: { name: 'StartTime' },
  		endTime: { name: 'EndTime' },
  		startTimezone: { name: 'StartTimezone' },
  		endTimezone: { name: 'EndTimezone' },
  		recurrenceRule: { name: 'RecurrenceRule' },
  		recurrenceID : { name: 'RecurrenceID' }
  	}
  };
  onPopupOpen(args: PopupOpenEventArgs): void {
  	console.log('onPopupOpen');
  }
  onEventClick(args: EventClickArgs): void {
      args.cancel = true;
      //console.log(args.event);
  	  this.openActivityDialog(args.event['Id']);
  }
  onNavigating(args: NavigatingEventArgs): void {
    //console.log(args);
    //args.cancel = true;
    if(args.action == 'date') {
        let currentNavigatingDate = args.currentDate.getFullYear()+''+(args.currentDate.getUTCMonth()+1);
        //console.log(this.currentCalendarYYYYMM, currentNavigatingDate, args.currentDate, new Date());
        //console.log(this.currentCalendarYYYYMM, currentNavigatingDate);

        if (this.currentCalendarYYYYMM != currentNavigatingDate) {
            this.clearCalendarSearchOptions(args.currentDate);
            this.currentCalendarYYYYMM = currentNavigatingDate;
            this.getCalendarTaskListNavi();
        }
        //console.log(this.calendarTaskListOptions);
    }
  }
  onCellDoubleClick(args: CellClickEventArgs): void {
  	args.cancel = true;
  }
  clickedCalendarSelectUsers(args: any): void {
    this.event.sendEvent({
      type: 'dialog',
      detail: 'select-users',
      target: 'crm-calendar-task',
      module: 'calendar'
    });
  }
  clickedTaskSelectUsers(args: any): void {
    this.event.sendEvent({
      type: 'dialog',
      detail: 'select-users',
      target: 'crm-calendar-task',
      module: 'task'
    });
  }
  clickedTaskSelectDates(args: any): void {
    this.event.sendEvent({
      type: 'dialog',
      detail: 'select-dates',
      target: 'crm-calendar-task',
      module: 'task-select-dates',
    });
  }
  clickedTaskExportTasks(args: any): void {
    let selectedRows = this.gridObj.getSelectedRecords();
    var ids = [];
    for (let data of selectedRows) {
      ids.push(data['Id']);
    }
    //console.log(ids);
    this.event.sendEvent({
      type: 'dialog',
      detail: 'export-tasks',
      target: 'crm-calendar-task',
      module: 'task',
      data: ids
    });
  }

  // Client Search
  public clientItems: any = [];
  public clientValues: string[] = [];
  public onClientValueChange(e: any) {
    setTimeout(()=>{
      //console.log(this.clientValues);
    });
  }

}
