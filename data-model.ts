import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";

export class Activity {
	Id: number;
	CreatedDate: string;
	Priority: string;
	CRM_Accounts: PersonInfoVM[];
	Regarding: string = '';
    ActivityType: string;
    ActivityStatus: number;
	ScheduledWith: PersonInfoVM[];
	CreatedBy: string;
	Recurrence: string;
	ScheduledForId: number;
	StartDate: string;
	StartTime: string;
	EndDate: string;
	EndTime: string;

	CreatedDateStd: Date;
	StartDateStd: Date;
	EndDateStd: Date;
	StartTimeStd: Date;
	EndTimeStd: Date;

	Comments: Comment[] = [];
	CRMFiles: CRMFile[] = [];

	IsMyRecord: boolean;
	IsFavourite: boolean;
    IsAllDay: boolean;
    
    ClientNames: string;
    ScheduledWithNames : string;
}

export class AdvisorCRM {
    SearchTemplatePersonalData: ClientSearchOption[];
    SearchTemplateClientFinancialInformation: ClientSearchOption[];
    SearchTemplateSuperannuation: ClientSearchOption[];
    SearchTemplateRiskProfile: ClientSearchOption[];
    SearchTemplateInvestmentStrategy: ClientSearchOption[];
    SearchTemplatePersonalInsurance: ClientSearchOption[];
    SearchTemplateEstatePlanning: ClientSearchOption[];
    SearchTemplateCompliance: ClientSearchOption[];
}

export class IPSPortfolioInformation {
    SearchTemplateAssets: ClientSearchOption[];
    SearchTemplateAssetClass: ClientSearchOption[];
    SearchTemplatePortfolio: ClientSearchOption[];
}